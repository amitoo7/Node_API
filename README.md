# Node_API
Node API with Express and Postgres

## Repositories
- https://gitlab.com/amitoo7/Node_API.git
- https://gitlab.com/amitoo7/Node_API_Deploy.git
 
## CD/CI Components


- Gitkab CI for Continuous Integration

- Helm for deployment manager

- ArgoCD for Continuous Deployment

![alt text](diagram/flow.png)
