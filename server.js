require('dotenv').config();

const app = require('../Node_API/app');

const port = 3000;
app.listen(port, () => {
  console.log(`App running on port ${port}.`);
});
