FROM node:12.13.0-alpine
RUN mkdir -p /opt/Node_API
WORKDIR /opt/Node_API
RUN adduser -S app
COPY . .
RUN npm install
RUN chown -R app /opt/Node_API
USER app
EXPOSE 3000
CMD [ "npm", "start" ]
